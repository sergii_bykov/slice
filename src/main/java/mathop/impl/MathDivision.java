package mathop.impl;


import mathop.MathFactory;

public class MathDivision implements MathFactory {

    @Override
    public double countTwoValue(double num1, double num2) {
        double result = (num1/num2)*1000_000;
        result = Math.round(result);
        result = result/1000_000;
        return result;
    }
}
