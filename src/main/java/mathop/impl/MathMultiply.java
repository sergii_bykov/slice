package mathop.impl;


import mathop.MathFactory;

public class MathMultiply implements MathFactory {
    @Override
    public double countTwoValue(double num1, double num2) {
        return num1*num2;
    }
}
