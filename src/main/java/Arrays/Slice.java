package Arrays;

public class Slice {
    public static int[] insertionSort(int[] arr) {
        {
            int j, value;

            for (int i = 1; i < arr.length; i++) {
                value = arr[i];
                for (j = i - 1; j >= 0 && arr[j] > value; j--) {
                    arr[j + 1] = arr[j];
                }
                arr[j + 1] = value;
            }
        }
        return arr;
    }


}

