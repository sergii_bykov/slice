package Arrays;

import org.junit.Test;

import java.sql.Array;

import static org.junit.Assert.assertArrayEquals;

public class SliceTest {
    @Test
    public void insertionSortTest() {
        int[] result = Slice.insertionSort(new int[]{5, 7, 1, 9, 12});
        assertArrayEquals(result, new int[]{1, 5, 7, 9, 12});

    }

}
