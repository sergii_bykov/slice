package regexnumber;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import regexhexnumber.RegexHexNumber;

public class RegexHexNumberTest {

    RegexHexNumber cut = new RegexHexNumber();

    static Arguments[] RegexHexNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, "4F81"),
                Arguments.arguments(false, "45g56"),
                Arguments.arguments(true, "111111"),
                Arguments.arguments(false, "sdFFsffFFY"),
                Arguments.arguments(true, "aB1")


        };
    }

    @ParameterizedTest
    @MethodSource("RegexHexNumberTestArgs")
    void RegexHexNumberTest(boolean expected, String hexNumber){
        boolean actual = cut.checkHexNumber(hexNumber);
        Assertions.assertEquals(expected, actual);
    }


}
