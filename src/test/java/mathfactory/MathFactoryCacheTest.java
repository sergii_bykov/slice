package mathfactory;


import mathop.MathFactory;
import mathop.cache.MathFactoryCache;
import mathop.impl.MathDivision;
import mathop.impl.MathMinus;
import mathop.impl.MathMultiply;
import mathop.impl.MathPlus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MathFactoryCacheTest {

    MathPlus mathPlus = new MathPlus();
    MathMinus mathMinus = new MathMinus();
    MathMultiply mathMultiply = new MathMultiply();
    MathDivision mathDivision = new MathDivision();

    MathFactory[] mathFactory = new MathFactory[]{mathPlus, mathMinus, mathMultiply, mathDivision};

    MathFactoryCache cut = new MathFactoryCache(mathFactory);


    static Arguments[] mathFactoryCacheTestArgs() {
        return new Arguments[]{
                Arguments.arguments(6.666667, "/", 20, 3),
                Arguments.arguments(20, "+", 17, 3),
                Arguments.arguments(35, "-", 40, 5),
                Arguments.arguments(50, "*", 10, 5),
                Arguments.arguments(0, "plus", 100, 20)

        };
    }

    @ParameterizedTest
    @MethodSource("mathFactoryCacheTestArgs")
    void mathFactoryCacheTest(double expected, String operation, double num1, double num2) {
        double actual = cut.getFactory(operation, num1, num2);
        Assertions.assertEquals(expected, actual);
    }

}
