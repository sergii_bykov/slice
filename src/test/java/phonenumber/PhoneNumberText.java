package phonenumber;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class PhoneNumberText {

    PhoneNumber cut = new PhoneNumber();


    static Arguments[] PhoneNumberTestArgs(){
        return new Arguments[]{
                Arguments.arguments("+380 97 412-46-41", true),
                Arguments.arguments("+7 214 156-34-56", true),
                Arguments.arguments("+1 324 789-d4-56", false),
                Arguments.arguments("+1 412 55-004-56", false),
                Arguments.arguments("+1 218 567-45-78", true),
                Arguments.arguments("+090 812 111-45-98", false)
        };
    }
    @ParameterizedTest
    @MethodSource("PhoneNumberTestArgs")
    void generateRandomNumberTest(String str,boolean expected){
        boolean actual=cut.PhoneNumber(str);
        Assertions.assertEquals(expected, actual);
    }
}
