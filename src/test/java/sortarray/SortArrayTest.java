package sortarray;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class SortArrayTest {

    SortArray cut = new SortArray();

    static Arguments[] sortTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new int[]{5,6,7,8,9}, new int[]{9,6,7,8,5}),
                Arguments.arguments(new int[]{1,2,3,4,5}, new int[]{3,5,4,1,2}),
        };
    }

    @ParameterizedTest
    @MethodSource("sortTestArgs")
    void sortBubble(int[] expected, int[] array){
        int[] actual = cut.sortArray(array);
        Assertions.assertArrayEquals(expected,actual);
    }

}
